<?php

use Migrations\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        //create users table
        $this->table('users')
            ->addColumn('username', 'string', ['limit' => 50])
            ->addColumn('email', 'string', ['limit' => 100])
            ->addColumn('password', 'string', ['limit' => 255])
            ->addColumn('first_name', 'string', ['limit' => 50])
            ->addColumn('last_name', 'string', ['limit' => 50])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime', ['null' => true])
            ->addColumn('status', 'boolean', ['default' => false])
            ->addColumn('email_hash', 'string', ['limit' => 255])
            ->addColumn('api_key_plain', 'string', ['limit' => 255])
            ->addColumn('api_key', 'string', ['limit' => 255])
            ->addIndex(['username', 'email'], ['unique' => true])
            ->create();
    }
}
